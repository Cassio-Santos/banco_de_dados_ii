CREATE DATABASE IF NOT EXISTS exerciciosOA;

use exerciciosOA;

CREATE TABLE cidade (
Idcidade INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
descricao VARCHAR(40) NOT NULL);

DESC cidade;

CREATE TABLE departamento (
Iddepartamento INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
descricao VARCHAR(40) NOT NULL,
telefone VARCHAR(20) NOT NULL,
Idcidade INT NOT NULL,
CONSTRAINT FK_departamento_cidade_idcidade FOREIGN KEY (Idcidade)
REFERENCES cidade (Idcidade)
);

DESC departamento;

CREATE TABLE funcionario (
Idfuncionario INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
Nome VARCHAR (50) NOT NULL,
Nascimento DATE NOT NULL,
Sexo ENUM ('M','F'),
Admissao DATE NOT NULL,
Salario DECIMAL (8,2),
Iddepartamento INT NOT NULL,
CONSTRAINT FK_funcionario_departamento FOREIGN KEY (Iddepartamento)
REFERENCES departamento (Iddepartamento)
);

DESC funcionario;

INSERT INTO cidade VALUES (1	,'Marília'),
						  (2 	,'Vera Cruz'),
						  (3	,'Pompeia'),
						  (4	,'Garça'),
						  (5	,'Bauru');
                          
INSERT INTO departamento VALUES (10,	'Diretoria',	'(14) 3433 1515',	1),
								(20,	'RH',	'(14) 3433 1516',	1),
								(30,	'TI',	'(14) 3492 1718',	2),
								(40,	'TI',	'(14) 3471 1821',	4),
								(50,	'Vendas',	'(14) 3471 1822',	4);
                                
INSERT INTO funcionario VALUES  (100,	'Ana',	'1980/05/15',	'F',	'2010/03/01',	'7500.00',	10),
								(200,	'Maria',	'1978/12/13',	'F',	'2012/09/15',	'3938.12',	10),
								(300,	'José',	'1997/03/08',	'M',	'2009/03/05',	'5200.00',	20),
								(400,	'Aparecido',	'1979/06/25',	'M',	'2011/04/10',	'7385.59',	30),
								(500,	'Marcia',	'1985/02/03',	'F',	'2010/03/01',	'3500.00',	40),
								(600,	'Orlando',	'1983/12/25',	'M',	'2012/06/10',	'2754.15',	20),
                                (600,	'Orlando',	'1983/12/25',	'M',	'2012/06/10',	'2754.15',	20);
                                
INSERT INTO funcionario (Nome, Nascimento, Sexo, Admissao, Salario, IdDepartamento)
    VALUES ('Orlando',	'1983/12/25',	'M',	'2012/06/10',	'2754.15',	20);					
                                
                                
UPDATE funcionario SET Salario = Salario * 1.1 
#WHERE (Iddepartamento = 10 OR Iddepartamento = 30);
WHERE Iddepartamento
IN (10,30);

show tables;

SELECT * FROM funcionario where (idfuncionario > 200 AND idfuncionario < 400);

select * from funcionario_x;

DESC funcionario_x;

SELECT f.nome,f.sexo, d.descricao
FROM funcionario AS f
INNER JOIN departamento AS d ON d.Iddepartamento = f.Iddepartamento;

SELECT c.idcidade, c.descricao AS cidade, d.descricao AS setor, d.telefone
FROM cidade AS c
LEFT OUTER JOIN departamento d
ON d.Idcidade = c.Idcidade
ORDER BY c.descricao;

CREATE INDEX i_fk_funcionario_departamento ON funcionario (iddepartamento);

CREATE OR REPLACE VIEW funcionariof AS
SELECT idfuncionario, salario, nome 
FROM funcionario WHERE sexo = 'F';

SELECT * FROM funcionariof;

SELECT d.descricao, (SELECT ROUND(SUM(salario),2)) FROM funcionario f 
RIGHT JOIN departamento d 
ON d.Iddepartamento = f.Iddepartamento
GROUP BY d.descricao;

SELECT c.descricao AS cidade, (SELECT COUNT(f.Idfuncionario))
AS Total_funcionario
FROM funcionario f
LEFT JOIN departamento d
ON f.Iddepartamento = d.Iddepartamento
LEFT JOIN cidade c
ON c.Idcidade = d.Idcidade
GROUP BY 1;

SELECT MAX(f.salario) AS maiorSalario FROM funcionario f
LEFT JOIN departamento d
ON d.Iddepartamento = f.Iddepartamento
LEFT JOIN cidade c
ON d.Idcidade = c.Idcidade; 