INSERT INTO grupo 
VALUES (1, 'Material de escritório');
INSERT INTO grupo (idgrupo,descricao) 
VALUES (2, 'Material de limpeza');
INSERT INTO grupo (idgrupo,descricao) 
VALUES (3, 'Informática');

SELECT * FROM grupo;

INSERT INTO produto 
VALUES (1,'Papel A4', '2017-08-21',1);
INSERT INTO produto 
VALUES (2,'Caneta Esferográfica Azul', '2017-08-21',1);
INSERT INTO produto 
VALUES (3,'Lápis Nº 2', '2017-08-21',1);
INSERT INTO produto 
VALUES (4,'Detergente', '2017-08-21',2);
INSERT INTO produto 
VALUES (5,'Vassoura', '2017-08-21',2);
INSERT INTO produto 
VALUES (6,'DVD', '2017-08-21',3);
INSERT INTO produto 
VALUES (7,'Pendrive 16 GB', '2017-08-21',3);
INSERT INTO produto 
VALUES (8,'Teclado ABNT', NOW(),3);

SELECT * FROM produto;
/*DELETE FROM produto WHERE idproduto = 8; */

INSERT INTO almoxarifado
VALUES (1,'Rua São Luiz, 520', '(14) 3433 1215');
INSERT INTO almoxarifado
VALUES (2,'Av. Sato Antônio, 1718', '(14) 3422 2085');
INSERT INTO almoxarifado (idalmoxarifado, endereco)
VALUES (3,'Rua Alvares Cabral, 6');


INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(1,1,1000);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(1,3,8);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(2,2,21);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(2,4,6);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(2,6,12);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(3,2,3);
INSERT INTO estoque (idalmoxarifado, idproduto, qtde) VALUES(3,7,1);