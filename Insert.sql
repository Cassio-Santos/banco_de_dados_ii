use aulas;

CREATE TABLE Juarez (idju INT(5) NULL,
					 Nome VARCHAR(50) NULL,
CONSTRAINT CK_Juarez_idju CHECK(
);


INSERT INTO Juarez (idju, Nome) VALUES (7, 'CAssio');

DROP TABLE IF EXISTS Persons;

CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    constraint CK_Age CHECK (Age>=18)
);

INSERT INTO Persons(ID, LastName, FirstName, Age) VALUES(1, 'Nascimento', 'Gabriel', 16);

ALTER TABLE Persons
ADD  constraint CK_Age CHECK (Age>=18);

ALTER TABLE Persons
ADD  constraint PK_Persons PRIMARY KEY (ID);

SELECT * FROM Persons;

DESC Persons;

select *
from information_schema.table_constraints WHERE table_name = 'Persons';