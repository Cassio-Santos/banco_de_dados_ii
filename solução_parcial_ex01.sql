CREATE DATABASE ex01;
use ex01;

CREATE TABLE cidade (
idcidade INT NOT NULL,
descricao VARCHAR(50) NOT NULL,
CONSTRAINT PK_Cidade PRIMARY KEY (idcidade)
);

CREATE TABLE departamento (
iddepartamento INT NOT NULL,
descricao VARCHAR(45) NOT NULL,
telefone VARCHAR(15),
idcidade INT NOT NULL,
CONSTRAINT PK_Departamento PRIMARY KEY (iddepartamento),
CONSTRAINT FK_Departamento FOREIGN KEY (idcidade) REFERENCES cidade (idcidade)
);


CREATE TABLE funcionario (
idfuncionario INT NOT NULL,
nome VARCHAR(80) NOT NULL,
nascimento DATE NOT NULL,
sexo CHAR(1) NOT NULL,
admissao DATETIME,
salario DECIMAL(8,2),
iddepartamento INT NOT NULL,
CONSTRAINT PK_Funcionario PRIMARY KEY (idfuncionario),
CONSTRAINT FK_Funcionario FOREIGN KEY (iddepartamento) 
REFERENCES departamento (iddepartamento)
);


INSERT INTO cidade (idcidade, descricao) VALUES (1, 'Marília');
INSERT INTO cidade (idcidade, descricao) VALUES (2, 'Vera Cruz');
INSERT INTO cidade (idcidade, descricao) VALUES (3, 'Pompéia');
INSERT INTO cidade (idcidade, descricao) VALUES (4, 'Garça');

SELECT * FROM cidade;

INSERT INTO departamento VALUES (10,'Diretoria','(14) 3433 1515',1);
INSERT INTO departamento VALUES (20,'RH','(14) 3433 1516',1);
INSERT INTO departamento VALUES (30,'TI','(14) 3492 1718',2);
INSERT INTO departamento VALUES (40,'TI','(14) 3471 1821',4);
INSERT INTO departamento VALUES (50,'Vendas','(14) 3471 1822',4);

SELECT * FROM departamento;

SELECT c.*, d.*
FROM cidade c RIGHT OUTER JOIN departamento d
 ON (c.idcidade = d.idcidade);