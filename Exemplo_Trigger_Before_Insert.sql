USE aulabdii;

CREATE TABLE aluno (id INT NOT NULL AUTO_INCREMENT,
					 nome VARCHAR(50) NOT NULL,
                     nota DECIMAL(4,2),
CONSTRAINT PK_Idaluno PRIMARY KEY (id));

INSERT INTO aluno VALUES (id,'cassio', 10);

SELECT * FROM aluno;

DELIMITER $$
CREATE TRIGGER tr_insert BEFORE INSERT ON aluno FOR EACH ROW
BEGIN
	IF(NEW.nota < 0) THEN
		SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Nao e permitido notas menores que Zero';
	END IF;
END
$$
DELIMITER ;