USE crm;

insert into cidade (id_cidade, descricao)
values (1, 'Bauru');
insert into cidade (id_cidade, descricao)
values (2, 'Mar�lia');
insert into cidade (id_cidade, descricao)
values (3, 'Vera Cruz');
insert into cidade (id_cidade, descricao)
values (4, 'Gar�a');
insert into cidade (id_cidade, descricao)
values (5, 'Tup�');
insert into cidade (id_cidade, descricao)
values (6, 'Pomp�ia');
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (1, 'Kamikaze Sports', null, 2);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (2, 'Loja de Esportes Record', null, 1);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (3, 'Karinas Sport', null, 4);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (4, 'Paraguay Shop', null, 2);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (5, 'Sempre Barato', null, 3);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (6, 'Wind Shop', null, 1);
insert into cliente (codigo_cliente, nome, email, id_cidade)
values (7, 'RJS Representa��es', null, 6);
insert into vendedor (codigo_vendedor, nome)
values (1, 'Ronaldo');
insert into vendedor (codigo_vendedor, nome)
values (2, 'Neymar');
insert into vendedor (codigo_vendedor, nome)
values (3, 'Lucas');
insert into vendedor (codigo_vendedor, nome)
values (4, 'Kl�ber');
insert into vendedor (codigo_vendedor, nome)
values (5, 'Rog�rio');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (1, '2017-03-12', 3, 5, 'Prospec��o');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (2, '2017-03-13', 6, 2, 'P�s-Venda');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (3, '2017-03-27', 6, 5, 'Prospec��o');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (4, '2017-03-10', 1, 3, 'Prospec��o');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (5, '2017-03-18', 5, 1, 'P�s-Venda');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (6, '2017-03-27', 4, 5, 'Suporte');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (7, '2017-03-05', 2, 2, 'Prospec��o');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (8, '2017-03-20', 7, 4, 'P�s-Venda');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (9, '2017-03-19', 1, 3, 'P�s-Venda');
insert into agenda (id_agenda, data, codigo_cliente, codigo_vendedor, descricao)
values (10, '2017-03-12', 5, 1, 'Suporte');
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (1, 3);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (2, 1);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (2, 5);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (3, 2);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (4, 6);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (5, 1);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (5, 2);
insert into vendedorcidade (codigo_vendedor, id_cidade)
values (5, 5);
