# comecando os teste com DDL e DML
CREATE DATABASE BdIIOa;

USE BdIIOa;

CREATE TABLE cidade(Idcidade INT AUTO_INCREMENT NOT NULL,
                    Descricao VARCHAR(50) NOT NULL
);

CREATE TABLE departamento(Iddepartamento INT (2) NOT NULL,
                          Descricao VARCHAR(50) NOT NULL,
                          Telefone VARCHAR(20) NOT NULL,
CONSTRAINT fk_cidade_departamento FOREIGN KEY (Idcidade) REFERENCES cidade (Idcidade)  
);

CREATE TABLE funcionarios(Idfuncionario INT(100) NOT NULL,
                          Nome VARCHAR(50) NOT NULL,	
                          Nascimento DATE NOT NULL,
                          Sexo ENUM('M','F'),
                          Admissao DATE NOT NULL,
                          Salario DECIMAL (8,2),
                          Iddepartamento INT NOT NULL,
CONSTRAINT fk_departamento_funcionario FOREIGN KEY (iddepartamento)
REFERENCES departamento(Iddepartamento)  
);