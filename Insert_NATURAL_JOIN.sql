CREATE DATABASE exercicios1;
USE exercicios1;

CREATE TABLE cidade(
idcidade INT (3) NOT NULL,
descricao VARCHAR (50),
CONSTRAINT cidade_pk PRIMARY KEY(idcidade)
);

CREATE TABLE departamento(
iddepartamento INT (4) NOT NULL,
descricao VARCHAR(45),
telefone VARCHAR(15),
idcidade INT NOT NULL,
CONSTRAINT departamento_pk PRIMARY KEY(iddepartamento),
CONSTRAINT cidade_departamento_fk FOREIGN KEY(idcidade) 
REFERENCES cidade (idcidade)
);

CREATE TABLE funcionario(
idfuncionario INT (3) NOT NULL,
nome VARCHAR(80),
nascimento DATE,
sexo CHAR(1),
admissao DATETIME,
salario DECIMAL(8,2),
iddepartamento INT NOT NULL,
CONSTRAINT funcionario_pk PRIMARY KEY (idfuncionario),
CONSTRAINT departamento_funcionario_fk FOREIGN KEY (iddepartamento) REFERENCES departamento(iddepartamento)
);


INSERT INTO cidade
VALUES (1 , 'Marília');
INSERT INTO cidade
VALUES (2 , 'Vera Cruz');
INSERT INTO cidade
VALUES (3 , 'Pompéia');
INSERT INTO cidade
VALUES (4 , 'Garça');

SELECT * FROM funcionario;

INSERT INTO departamento
VALUES(10,	'Diretoria', '(14) 3433 1515',	1);
INSERT INTO departamento
VALUES(20, 'RH', '(14) 3433 1516' , 1);
INSERT INTO departamento
VALUES(30, 'TI', '(14) 3492 1718',	2);
INSERT INTO departamento
VALUES(40, 'TI' , '(14) 3471 1821'	, 4);
INSERT INTO departamento
VALUES(50,	'Vendas',	'(14) 3471 1822' , 4);


SELECT * FROM cidade;

INSERT INTO funcionario
VALUES (100, 'Ana' , '1994-05-15' , 'F' , '2016-03-01' , '7500.00' , 10); 
INSERT INTO funcionario
VALUES (200, 'Maria' , '1988-12-13' , 'F' , '2015-09-15' , '3938.12' , 10); 
INSERT INTO funcionario
VALUES (300, 'José' , '1987-03-08' , 'M' , '2014-03-05	' , '5200.00' , 20);
INSERT INTO funcionario
VALUES (400, 'Aparecido' , '1989-06-25' , 'M' , '2016-04-10' , '7385.59' , 30);
INSERT INTO funcionario
VALUES (500, 'Marcia' ,	'1995-02-03' , 'F' , '2015-03-01' ,	'3500.00' ,	40);
INSERT INTO funcionario
VALUES (600, 'Orlando'	, '1993-12-25',	'M' , '2017-06-10' , '2754.15' ,20);

SELECT * FROM departamento;

SELECT iddepartamento, descricao, idcidade FROM departamento	
WHERE descricao = 'TI';

SELECT nome, sexo FROM funcionario
WHERE iddepartamento >= 20;

USE exercicios1;

SELECT funcionario.*, cidade.descricao
FROM funcionario
NATURAL JOIN cidade;