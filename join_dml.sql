use aulajoin;

INSERT INTO cidade (cod_cidade, local) VALUES (1000, 'Marília');
INSERT INTO cidade (cod_cidade, local) VALUES (3000, 'Bauru');
INSERT INTO cidade (cod_cidade, local) VALUES (4000, 'Botucatu');


INSERT INTO pessoa VALUES (1,'Carlos',24,1700,219473659,1000);
INSERT INTO pessoa VALUES (2,'José',23,1500,227379573,4000);


INSERT INTO empresa (id_empresa,nome,cnpj,ie,telefone,cod_cidade,id_gerente) VALUES (50, 'RJS Tecnologia','24.454.242/0001-01',1524213,'235273642',3000,2);
INSERT INTO empresa (id_empresa,nome,cnpj,ie,telefone,cod_cidade) VALUES (67, 'Scan Sistemas','18.182.801/0001-01',1823427,'248339192',1000);
INSERT INTO empresa (id_empresa,nome,cnpj,ie,telefone,cod_cidade,id_gerente) VALUES (85, 'TransHard','23.021.901/0001-01',1398014,'215374591',4000,1);

