CREATE DATABASE aulas;

use aulas;
CREATE TABLE cliente(idcliente INT NOT NULL AUTO_INCREMENT,
					nome VARCHAR(45)NOT NULL,
					cpf VARCHAR (15) NOT NULL,
                    telefone VARCHAR (15) NOT NULL,
                    endereco VARCHAR (45) NOT NULL,
                    email VARCHAR (45) NOT NULL,
                    datanasc DATE NOT NULL,
CONSTRAINT PK_Cliente PRIMARY KEY (idcliente)
);

UPDATE cliente SET nome = 'Ricardo' WHERE idcliente = 1;

SELECT * FROM cliente;

UPDATE cliente SET idcliente = idcliente WHERE nome = 'Ricardo' AND cpf = '12345';