
CREATE DATABASE trabalhopo;
USE trabalhopo;

-- 1 TABELAS
CREATE TABLE departamento (
    iddepartamento INT AUTO_INCREMENT NOT NULL,
    descricao VARCHAR(45) NOT NULL,
    CONSTRAINT PRIMARY KEY (iddepartamento)
);

CREATE TABLE funcionario (
    idfuncionario INT AUTO_INCREMENT NOT NULL,
    nome VARCHAR(80) NOT NULL,
    sexo CHAR(1) NOT NULL,
    admissao DATETIME NOT NULL,
    salario DECIMAL(8 , 2 ) NOT NULL,
    iddepartamento INT NOT NULL,
    CONSTRAINT PRIMARY KEY (idfuncionario),
    CONSTRAINT FK_iddepartamento FOREIGN KEY (iddepartamento)
        REFERENCES departamento (idpdepartamento)
);

CREATE TABLE logdepartamento (
    idlogdepartamento INT NOT NULL AUTO_INCREMENT,
    idfuncionario INT NOT NULL,
    iddeptoanterior INT NOT NULL,
    iddeptonovo INT NOT NULL,
    data_log DATETIME NOT NULL,
    CONSTRAINT PRIMARY KEY (idlogdepartamento),
    CONSTRAINT FK_log_iddepto_funcionario FOREIGN KEY (idfuncionario)
        REFERENCES funcionario (idfuncionario),
    CONSTRAINT FK_iddeptoanterior FOREIGN KEY (iddeptoanterior)
        REFERENCES departamento (iddepartamento),
    CONSTRAINT FK_iddeptonovo FOREIGN KEY (iddeptonovo)
        REFERENCES departamento (iddepartamento)
);

CREATE TABLE logsalario (
    idlogsalario INT NOT NULL AUTO_INCREMENT,
    idfuncionario INT NOT NULL,
    valoranterior DECIMAL(8 , 2 ),
    valornovo DECIMAL(8 , 2 ),
    data_logsalario DATETIME NOT NULL,
    CONSTRAINT PRIMARY KEY (idlogsalario),
    CONSTRAINT FK_idfuncionario FOREIGN KEY (idfuncionario)
        REFERENCES funcionario (idfuncionario)
);


-- 1 TRIGGERS
DELIMITER $$ 
CREATE TRIGGER alteracao_salario BEFORE UPDATE 
ON funcionario
FOR EACH ROW
	BEGIN
		INSERT INTO logsalario (idlogsalario,idfuncionario,valoranterior,valornovo,data_logsalario)
		VALUES(	idlogsalario,
				OLD.idfuncionario,
				OLD.salario,
				NEW.salario,
				NOW());
	END; $$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER alteracao_departamento BEFORE UPDATE 
ON funcionario
FOR EACH ROW
	BEGIN
		INSERT INTO logdepartamento (idlogdepartamento,idfuncionario,iddeptoanterior,iddeptonovo,data_log)
		VALUES(	idlogdepartamento,
				OLD.idfuncionario,
				OLD.iddepartamento,
				NEW.iddepartamento,
				NOW());
	END; $$
DELIMITER ;


-- 2

CREATE USER 'ricardocassio'@'192.168.1.22' IDENTIFIED BY '123456';
GRANT SELECT, UPDATE, INSERT ON departamento TO ricardocassio@192.168.1.22 IDENTIFIED BY '123456';
GRANT SELECT, UPDATE, DELETE ON funcionario TO ricardocassio@192.168.1.22 IDENTIFIED BY '123456';
GRANT SELECT ON logdepartamento TO ricardocassio@192.168.1.22 IDENTIFIED BY '123456';
GRANT SELECT ON logsalario TO ricardocassio@192.168.1.22 IDENTIFIED BY '123456';

-- 3
CREATE INDEX i_nome_funcionario ON funcionario (nome);

-- 4
DELIMITER $$
CREATE PROCEDURE INSFUNC (IN idfuncionario INT, IN nome VARCHAR(80), IN sexo CHAR(1), IN admissao DATETIME, IN salario DECIMAL(8,2), IN iddepartamento INT)  
BEGIN	
    INSERT INTO funcionario VALUES(
		idfuncionario,
		nome,
		sexo, 
        admissao,
		salario,
        iddepartamento);	
END $$
DELIMITER ;

-- 5
DELIMITER $$
CREATE PROCEDURE ALTSALARIO (IN pidfun INT, IN salar DECIMAL (8,2))
BEGIN
	UPDATE funcionario 
    SET salario = salar 
    WHERE idfuncionario = pidfun;
END $$
DELIMITER ;

-- 6
CREATE VIEW SALDEPT AS
SELECT f.iddepartamento, d.descricao, SUM(salario) FROM funcionario f
JOIN departamento d ON (f.iddepartamento = d.iddepartamento)
GROUP BY iddepartamento;




