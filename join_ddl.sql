CREATE DATABASE aulajoin;
USE aulajoin;

Create table cidade (
cod_cidade int(4) not null,
local varchar(50) not null,
constraint PK_Cidade Primary Key (cod_cidade)
);

create table pessoa(
id_pessoa int(5) not null,
nome varchar(40) not null,
idade int(2) not null,
salario decimal(8,2) not null,
telefone varchar(15),
cod_cidade int(4) not null,
Constraint PK_Pessoa Primary Key(id_pessoa),
Constraint FK_Pessoa_Cidade Foreign Key (cod_cidade)
  References cidade (cod_cidade)
);

create table empresa(
id_empresa int(5) not null,
nome varchar(40) not null,
cnpj varchar(20) not null,
ie   int(7) not null,
telefone varchar(15),
cod_cidade int(4) not null,
id_gerente int(5),
Constraint PK_Empresa Primary Key(id_empresa),
Constraint FK_Empresa_Pessoa Foreign Key (id_gerente)
  References pessoa (id_pessoa),
Constraint FK_Empresa_Cidade Foreign Key (cod_cidade)
  References cidade (cod_cidade)
);
