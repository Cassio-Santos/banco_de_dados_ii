CREATE DATABASE crm;
USE crm;

create table cidade (
id_cidade Int(4) not null,
descricao varchar(50) not null,
constraint PK_Cidade Primary Key (id_cidade)
);

create table cliente(
codigo_cliente int(5) not null,
nome varchar(40) not null,
email varchar(50),
id_cidade int(4) not null,
Constraint PK_Cliente Primary Key(codigo_cliente),
Constraint FK_Cliente_Cidade Foreign Key (id_cidade)
  References cidade (id_cidade)
);

create table vendedor(
codigo_vendedor int(5) not null,
nome varchar(40) not null,
Constraint PK_Vendedor Primary Key(codigo_vendedor)
);

create table vendedorcidade(
codigo_vendedor int(5) not null,
id_cidade int(4) not null,
Constraint PK_VendedorCidade Primary Key (codigo_vendedor, id_cidade)
);

create table agenda(
id_agenda int(6) not null,
data date not null,
codigo_cliente int(5) not null,
codigo_vendedor int(5) not null,
descricao varchar(50),
Constraint PK_Agenda Primary Key(id_agenda),
Constraint FK_Agenda_Cliente Foreign Key (codigo_cliente)
  References Cliente (codigo_cliente),
Constraint FK_Agenda_Vendedor Foreign Key (codigo_vendedor)
  References Vendedor (codigo_vendedor)
);