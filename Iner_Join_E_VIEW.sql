/*CORREÇÃO DA OA*/

CREATE DATABASE correcaoOA;
USE correcaoOA;
/*Questão 01*/
CREATE TABLE peca (idpecas INT PRIMARY KEY NOT NULL,
				   descricao VARCHAR(50) NOT NULL);

CREATE TABLE cliente(idcliente INT NOT NULL,
					nome VARCHAR (80) NOT NULL,
                    data_cadastro DATE NOT NULL,
                    limite DECIMAL (8,2) NOT NULL,
CONSTRAINT PK_Cliente PRIMARY KEY (idcliente)
);

/*Questão 02*/

CREATE TABLE venda (idcliente INT NOT NULL,
					idpecas INT NOT NULL,
                    data_venda DATE NOT NULL,
                    quantidade INT NOT NULL,
                    valor_unitario DECIMAL (8,2) NOT NULL,
CONSTRAINT FK_Venda_Cliente FOREIGN KEY (idcliente) REFERENCES cliente(idcliente),
CONSTRAINT FK_Venda_Peca FOREIGN KEY (idpecas) REFERENCES peca (idpecas),
CONSTRAINT PK_Venda PRIMARY KEY (idcliente, idpecas, data_venda)                    
);

/*QUESTÃO 03*/
INSERT INTO cliente VALUES (1,'Alaor Cosntantino','2015-08-15','8953.20'),
						   (5,'Bete Freitas','2016-03-01','2500.00'),
						   (10,'Maria Aparecida da Luz','2017-09-20','13000.00');

/*QUESTÃO 04*/
UPDATE venda SET valor_unitario = 5.28
			  WHERE (data_venda BETWEEN '2017-01-01' AND '2017-12-31' OR idpecas = 93);
              
/*QUESTÃO 05*/
/*PRIMEIRA FORMA DE SE FAZER*/
SELECT SUM(quantidade*valor_unitario)
FROM venda
WHERE data_venda BETWEEN '2017-09-01' AND '2017-09-30'
AND idcliente = 5;

/*SEGUNDA FORMA DE SE FAZER*/

SELECT SUM(quantidade*valor_unitario)
FROM venda
WHERE data_venda >='2017-09-01' 
AND data_venda <='2017-09-30'
AND idcliente = 5;

/*TERCEIRA FORMA DE SE FAZER*/

SELECT SUM(quantidade*valor_unitario)
FROM venda
WHERE (data_venda)= 2017 
AND (data_venda)= 09
AND idcliente = 5;

/*QUESTÃO 06*/
SELECT c.nome, data_venda, descricao, quantidade, valor_unitario
FROM cliente c
JOIN venda v ON c.idcliente = v.idcliente
JOIN peca p ON p.idpecas = v.idpecas
WHERE p.descricao = 'carburador';

/*QUESTÃO 07*/
CREATE INDEX i_fk_idcliente_venda ON venda (idcliente);
CREATE INDEX i_fk_idpecas_venda ON venda (idpecas);

/*QUESTÃO 08*/

CREATE OR REPLACE VIEW PECASVENDIDAS
AS SELECT descricao, IFNULL(SUM(quantidade),0) AS SOMA
FROM venda RIGHT JOIN peca ON venda.idpecas = peca.idpecas
GROUP BY descricao;

SELECT * FROM PECASVENDIDAS;